const { initDB } = require("./database/config");
const Mark = require("./database/models/mark.model");
const Student = require("./database/models/student.model");
const fs = require("fs");

async function bootstrap() {
  await initDB();
  const studentList = await Student.findAll({
    include: [
      {
        model: Mark,
        where: {
          mark: 5,
        },
      },
    ],
  });

  const result = studentList.map((student) => {
    let j = 0;
    for (let i = 0; i < student.marks.length; i++) {
      if (student.marks[i] === 5) j++;
    }
    if (student.marks.length === j) {
      return student;
    }
  });

  fs.writeFileSync(
    "./output.json",
    JSON.stringify({
      result,
    })
  );
}

bootstrap();

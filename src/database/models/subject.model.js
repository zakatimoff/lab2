const { sequelize } = require("..");
const { Sequelize } = require("sequelize");

class Subject extends Sequelize.Model {}

Subject.init(
  {
    id: {
      type: Sequelize.DataTypes.UUID,
      defaultValue: Sequelize.DataTypes.UUIDV4,
      primaryKey: true,
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false,
    },
  },
  { sequelize: sequelize, modelName: "subject" }
);

module.exports = Subject;
